package ictgradschool.industry.arrays.printpattern;

public class Pattern {
    int number;
    char symbol;

    public Pattern(int count, char symbol) {
        this.number = count;
        this.symbol = symbol;
    }

    public int getNumberOfCharacters() {
        return number;
    }

    public void setNumberOfCharacters(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        String line = "";

        for (int i = 0; i < number; i++){
            line += symbol;
        }

        return line;
    }
}








        /*
        Pattern top = new Pattern(15, '*');

        Pattern sideOfFirstLine = new Pattern(7, '#');
        Pattern sideOfLine = new Pattern(7, '~');
        Pattern middle = new Pattern(1, '.');

        System.out.println(top);
        System.out.println(sideOfFirstLine.toString() + middle.toString() + sideOfFirstLine.toString());

        for (int i = 0; i < 6; i++) {
            middle.setNumberOfCharacters(middle.getNumberOfCharacters() + 1);
            System.out.println(sideOfLine.toString() + middle.toString() + sideOfLine.toString());
        }*/
