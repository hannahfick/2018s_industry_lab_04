package ictgradschool.industry.arrays.mobilephones;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    String brand;
    String model;
    double price;

    public MobilePhone(String brand, String model, double price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }


    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }


    public double getPrice() {
        return price;
    }

    public String getModel() {
        return model;

    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return brand + " " + model + " which cost $" + price;
    }

    @Override
    public boolean equals(Object other) {

        if (other instanceof MobilePhone) {
            MobilePhone otherM = (MobilePhone) other;

            return this.brand.equals(otherM.brand) && this.model.equals(otherM.model) && this.price == otherM.price;

        }
        return false;
    }
    public boolean isCheaperThan(MobilePhone other) {

        if (this.price < other.price) {
            return true;
        }
        return false;

    }


}


// TODO Insert isCheaperThan() method here



