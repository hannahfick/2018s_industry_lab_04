package ictgradschool.industry.arrays.lecturers;

import ictgradschool.industry.arrays.mobilephones.MobilePhone;

public class Lecturer {

    // instance variables
    private String name;
    private int staffId;
    private String[] papers;
    private boolean onLeave;
    
    public Lecturer(String name, int staffId, String[] papers, boolean onLeave) {
        this.name = name;
        this.staffId = staffId;
        this.papers = papers;
        this.onLeave = onLeave;
    }
        public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getStaffId() {
        return staffId;
    }
    public String[] getPapers() {
        return papers;
    }
    public void setPapers(String[] papers) {
        this.papers = papers;
    }
    public boolean isOnLeave () {
           return this.onLeave;

    }

    public void setOnLeave(boolean onLeave) {
        this.onLeave = onLeave;
    }

    @Override
    public String toString() {
        return "id: " + staffId + " " + name + " is teaching " + papers.length + " papers.";
    }



    public boolean teachesMorePapersThan (Lecturer other) {

        if (this.papers.length > other.papers.length){

           return true;
        }
        return false;

    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }
}



